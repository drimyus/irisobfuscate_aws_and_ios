import os
import cv2
import dlib as dlib
import math
import numpy as np

from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename


"""
*-------------------------------------------------------*
|                   Flask Sever Session                 |
*-------------------------------------------------------*
"""
# Initialize the Flask application
app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


# This route will show a form to perform an AJAX request jQuery is loaded to execute the request and update the
# value of the operation
@app.route('/')
def index():
    return render_template('index.html')


# Route that will process the file upload
@app.route('/uploader', methods=['POST'])
def upload():
    # Get the name of the uploaded file

    file = request.files['file']

    # Make the filename safe, remove unsupported chars
    filename = secure_filename(file.filename)

    # Check if the file is one of the allowed types/extensions
    if file and allowed_file(file.filename):

        try:
            print 'uploading...'

            # Remove all files in uploader directory
            file_list = os.listdir(app.config['UPLOAD_FOLDER'])
            for fname in file_list:
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], fname))

            # Move the file form the temporal folder to the upload folder we setup
            detected_filename = "detected_" + filename
            print 'filename : {} -> {}'.format(filename, detected_filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], detected_filename))

            # Processing the face image for iris_obfuscating
            print 'processing...'
            msg, filepath = lib_detection(os.path.join(app.config['UPLOAD_FOLDER'], detected_filename))
            print 'successfully uploaded.'

            if filepath is None:
                return msg
                # return send_from_directory(directory='uploads', filename=filename)
            else:
                # return msg
                print 'downloading...'
                return send_from_directory(directory='uploads', filename=detected_filename)
                # Redirect the user to the uploaded_file route, which will basicaly show on the browser the uploaded file
                # return redirect(url_for('uploaded_file', filename=filename))

        except Exception as e:
            print 'Exception: {}'.format(e)
            return 'There is an error in uploading {} file.'.format(detected_filename)

    else:
        print 'Not allowed the file type : {}'.format(filename)
        return 'Not allowed the file type : {}'.format(filename)


# This route is expecting a parameter containing the name of a file. Then it will locate that file on the upload
# directory and show it on the browser, so if the user uploads an image, that image is going to be show after the upload
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    print 'uploads'
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)



"""
*-------------------------------------------------------*
|                   iris obfuscate Session              |
*-------------------------------------------------------*
"""
PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"

LIP_POINTS = list(range(48, 68))

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(PREDICTOR_PATH)


def read_im_and_landmarks(im):

    rects = detector(im, 1)

    points = []
    if len(rects) > 1:
        return -1
        # raise TooManyFaces
    if len(rects) == 0:
        return -2
        # raise NoFaces

    for p in predictor(im, rects[0]).parts():
        points.append((p.x, p.y))

    return points


# Fuction for iris_obcuscating
def lib_detection(filepath, thr=30, win_sz=7):

    image = cv2.imread(filepath, cv2.IMREAD_COLOR)
    res = image.copy()

    landmarks = read_im_and_landmarks(image)
    if landmarks == -1:
        print 'Too many faces in this photo.'
        return 'Too many faces in this photo.', None
    elif landmarks == -2:
        print 'No face in this photo.'
        return 'No face in this photo.', None
    else:

        c = 0
        for (a, b) in landmarks:

            if c in LIP_POINTS:
                color_point = (0, 255, 255)
                cv2.circle(image, (a, b), 3, color_point, -1)
            c += 1

        cv2.imwrite(filepath, image)
        print 'The image {} is detected successfully and safe for internet use.'.format(filepath)
        return 'The image is detected successfully and safe for internet use.', filepath


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int(os.environ.get("PORT", 5000)),
        debug=True,
        threaded=True,
    )




